#!/bin/bash

REGISTRY=
TAG=
DEPCONFDIR=/tmp/deployconfig

while getopts "r:o:t:c:h?" args; do
    case "${args}" in
        r) REGISTRY=$OPTARG
            ;;
        o) ORG=$OPTARG
            ;;
        t) TAG=$OPTARG
            ;;
        c) CERTSDIR=$OPTARG
            ;;
        h) echo $(basename $0) \[-r image_registry_url\] \[-t image_tag_to_pull\]
            echo
            echo Defaults are registry=quay.io and tag=latest
            echo If -r and/or -t is given, default deployment yaml files will be copyed to
            echo $DEPCONFDIR with updated image locations. 
            echo 
            exit 1
            ;;
    esac
done

if [[ -d /etc/letsencrypt ]]; then
    CERTSDIR=/etc/letsencrypt
else
    CERTSDIR=$SNAP_DATA
fi

set -e
set -x

. $SNAP/bin/configure.sh

require k8s.service.subnet
require k8s.dns.ip
require k8s.rook.nodes
require k8s.rook.metadata.replicas
require k8s.rook.metadata.servers
require k8s.rook.data.replicas
require k8s.rook.data.failuredomain
require k8s.merge.svcnodes
require merge.etcd.volume.size

require_file $SNAP_DATA/k8s/mycluster.yml

export PATH=$SNAP/bin:$PATH
export PYTHONPATH=$SNAP/usr/lib/python3/dist-packages

until kubectl get componentstatus; do
  echo "waiting for k8s to become ready"
  sleep 1
done

#rbac rules
kubectl apply -f $SNAP/etc/k8s/kube-rbac.yml
kubectl apply -f $SNAP/etc/k8s/kube-rbac-bind.yml

#dns setup
kubectl config set-cluster mergetb \
  --certificate-authority=$SNAP_DATA/keychain/ca.pem \
  --embed-certs=true \
  --server=https://px:6443 \
  --kubeconfig=$SNAP_DATA/k8s/kubeconfig

export service_cidr=${CONFIG[k8s.service.subnet]}
export cluster_dns=${CONFIG[k8s.dns.ip]}
envsubst < $SNAP/etc/k8s/coredns.yml > $SNAP_DATA/k8s/coredns.yml

kubectl apply -f $SNAP_DATA/k8s/coredns.yml

#key config-maps
mkdir -p $SNAP_DATA/apicert
mkdir -p $SNAP_DATA/launchcert
mkdir -p $SNAP_DATA/wgcert
mkdir -p $SNAP_DATA/dbcert
mkdir -p $SNAP_DATA/mdbcert
mkdir -p $SNAP_DATA/xdccert
mkdir -p $SNAP_DATA/wgclicert

cp $CERTSDIR/keychain/live/api.mergetb.net/cert.pem       $SNAP_DATA/apicert/api_cert.pem
cp $CERTSDIR/keychain/live/api.mergetb.net/privkey.pem    $SNAP_DATA/apicert/api_key.pem
cp $SNAP_DATA/keychain/live/wg.mergetb.net/cert.pem      $SNAP_DATA/apicert/wgdcoord.pem

cp $CERTSDIR/keychain/live/launch.mergetb.net/cert.pem        $SNAP_DATA/launchcert/server.pem
cp $CERTSDIR/keychain/live/launch.mergetb.net/privkey.pem     $SNAP_DATA/launchcert/server-key.pem

cp $SNAP_DATA/keychain/ca.pem     $SNAP_DATA/dbcert/
cp $SNAP_DATA/keychain/db.pem     $SNAP_DATA/dbcert/
cp $SNAP_DATA/keychain/db-key.pem $SNAP_DATA/dbcert/

cp $SNAP_DATA/keychain/ca.pem               $SNAP_DATA/wgcert/
cp $SNAP_DATA/keychain/worker-wgd.pem       $SNAP_DATA/wgcert/
cp $SNAP_DATA/keychain/live/wg.mergetb.net/cert.pem       $SNAP_DATA/wgcert/wgdcoord.pem
cp $SNAP_DATA/keychain/live/wg.mergetb.net/privkey.pem    $SNAP_DATA/wgcert/wgdcoord-key.pem

cp $SNAP_DATA/keychain/live/wg.mergetb.net/cert.pem      $SNAP_DATA/xdccert/wgdcoord.pem
cp $CERTSDIR/keychain/live/xdc.mergetb.io/cert.pem       $SNAP_DATA/xdccert/xdc.pem
cp $CERTSDIR/keychain/live/xdc.mergetb.io/privkey.pem    $SNAP_DATA/xdccert/xdc-key.pem

cp $SNAP_DATA/keychain/live/wg.mergetb.net/cert.pem      $SNAP_DATA/wgclicert/wgdcoord.pem

kubectl create configmap merge-config --from-file=$SNAP/etc/k8s/merge.yml --from-file=$SNAP/etc/k8s/policy.yml
kubectl create configmap merge-api-cert --from-file=$SNAP_DATA/apicert
kubectl create configmap merge-launch-cert --from-file=$SNAP_DATA/launchcert
kubectl create configmap merge-db-cert --from-file=$SNAP_DATA/dbcert
kubectl create configmap merge-wg-cert --from-file=$SNAP_DATA/wgcert
kubectl create configmap merge-wgdcoord-cli-cert --from-file=$SNAP_DATA/wgclicert

cp $SNAP_DATA/keychain/ca.pem      $SNAP_DATA/mdbcert/
cp $SNAP_DATA/keychain/mdb.pem     $SNAP_DATA/mdbcert/
cp $SNAP_DATA/keychain/mdb-key.pem $SNAP_DATA/mdbcert/
kubectl create configmap merge-mdb-cert --from-file=$SNAP_DATA/mdbcert

# rook

## pre-requisites
kubectl apply -f $SNAP/etc/rook/pod-security-policy.yml
kubectl apply -f $SNAP/etc/rook/cluster-role.yml
kubectl apply -f $SNAP/etc/rook/cluster-role-binding.yml
kubectl apply -f $SNAP/etc/rook/role-bindings.yml

export METADATA_POOL_SIZE=${CONFIG[k8s.rook.metadata.replicas]}
export METADATA_SERVER_COUNT=${CONFIG[k8s.rook.metadata.servers]}
export DATA_POOL_SIZE=${CONFIG[k8s.rook.data.replicas]}
export FAILURE_DOMAIN=${CONFIG[k8s.rook.data.failuredomain]}

## mark which nodes are rook targets (presumably ones with dedicated storage disks)
for x in ${CONFIG[k8s.rook.nodes]//,/ }; do
  kubectl label nodes $x role=storage-node

  # this seems to be broken on rook at the moment
  #kubectl taint nodes $x storage-node=$x:NoSchedule
done

## install the rook operator
kubectl create -f $SNAP/etc/rook/operator.yml

cluster_config  > $SNAP_DATA/k8s/cluster.yml
## stand up the rook cluster
until kubectl apply -f $SNAP_DATA/k8s/cluster.yml; do
  sleep 1
done

envsubst < $SNAP/etc/rook/filesystem.yml > $SNAP_DATA/k8s/filesystem.yml
kubectl apply -f $SNAP_DATA/k8s/filesystem.yml

## define a block storage class
kubectl create -f $SNAP/etc/rook/rook-block-storage-class.yml


# etcd
#kubectl create -f $SNAP/etc/k8s/etcd-pv.yml
export SIZE=${CONFIG[merge.etcd.volume.size]}
envsubst < $SNAP/etc/k8s/etcd-pv-claim.yml > $SNAP_DATA/k8s/etcd-pv-claim.yml
kubectl create -f $SNAP_DATA/k8s/etcd-pv-claim.yml
kubectl apply  -f  $SNAP/etc/k8s/etcd.yml

# merge

## mark which nodes are merge service node targets
for x in ${CONFIG[k8s.merge.svcnodes]//,/ }; do
  kubectl label nodes $x role=merge-svc-node
done

# create xdc namespaces
kubectl create -f $SNAP/etc/k8s/merge/xdc/xdc-namespace.yml
kubectl create -f $SNAP/etc/k8s/merge/xdc/xdc-system-namespace.yml
kubectl create -f $SNAP/etc/k8s/merge/xdc/jumpc.yml

# let xdcs talk to wgdcoord in defaule namespace.
kubectl apply -f $SNAP/etc/k8s/merge/xdc/wg-endpoint.yml 

# set up permissions to control persistent volume claims in xdc namespace
kubectl apply -f $SNAP/etc/k8s/xdc-controller.yml
kubectl apply -f $SNAP/etc/k8s/merge/xdc/xdc-namespace.yml
kubectl apply -f $SNAP/etc/k8s/merge/xdc/xdc-operator.yml
kubectl apply -f $SNAP/etc/k8s/merge/xdc/jumpc.yml
kubectl apply -f $SNAP/etc/k8s/merge/xdc/xdc-web-proxy.yml

kubectl create configmap merge-xdc-cert --from-file=$SNAP_DATA/xdccert -n xdc

# deploy merge core services. If args have been given which modify the 
# default config, copy the yml files and substitute the given args.
if [[ -z $REGISTRY && -z  $TAG ]]; then 
    # use defaults.
    for x in $SNAP/etc/k8s/merge/*.yml; do
        kubectl apply -f $x
    done
else
    # copy and update config before apply
    FROMDIR=${FROMDIR:-/snap/merge-t2/current/etc/k8s/merge} 
    mkdir -p $DEPCONFDIR
    REGISTRY=${REGISTRY:-quay.io}
    TAG=${TAG:-latest}
    ORG=${ORG:-mergetb}

    for f in $FROMDIR/*.yml $FROMDIR/xdc/*.yml; do
        fullpath=$DEPCONFDIR/$(basename $f)

        # TBUI and wgdcoord are not built or maintained with the portal - they are not part of a tagged deployment.
        if [[ $(basename $f) != "tbui.yml" && $(basename $f) != "wgdcoord.yml" ]]; then 
            # woo regex (now I've got three problems). substitute in our registry, org and tag.
            sed "s/image: \"[^\/]*\/[^\/]*\/\([^:]*\):[^\"]*\"/image: \"$REGISTRY\/$ORG\/\1:$TAG\"/g" $f > $fullpath
        else
            cp $f $fullpath
        fi

        kubectl apply -f $fullpath
    done
fi
