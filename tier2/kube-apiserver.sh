#!/bin/bash

. $SNAP/bin/configure.sh

require   ip
require   k8s.service.subnet
require   k8s.dns.ip
require   ha

ac="Initializers,NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota"

if [[ ${CONFIG[ha]} == "yes" ]]; then
  etcd_cluster="https://ma0:2379,https://ma1:2379,https://ma2:2379"
else
  etcd_cluster="https://ma0:2379"
fi

mkdir -p $SNAP_COMMON/k8s

$SNAP/bin/kube-apiserver \
  --admission-control               $ac \
  --advertise-address               ${CONFIG[ip]} \
  --allow-privileged                true \
  --apiserver-count                 3 \
  --audit-log-maxage                30 \
  --audit-log-maxbackup             3 \
  --audit-log-maxsize               100 \
  --audit-log-path                  $SNAP_COMMON/k8s/audit.log \
  --authorization-mode              Node,RBAC \
  --bind-address                    0.0.0.0 \
  --client-ca-file                  $SNAP_DATA/keychain/ca.pem \
  --enable-swagger-ui=true          \
  --etcd-cafile                     $SNAP_DATA/keychain/ca.pem \
  --etcd-certfile                   $SNAP_DATA/keychain/db.pem \
  --etcd-keyfile                    $SNAP_DATA/keychain/db-key.pem \
  --etcd-servers                    $etcd_cluster \
  --event-ttl                       1h \
  --insecure-bind-address           127.0.0.1 \
  --kubelet-certificate-authority   $SNAP_DATA/keychain/ca.pem \
  --kubelet-client-certificate      $SNAP_DATA/keychain/kubernetes.pem \
  --kubelet-client-key              $SNAP_DATA/keychain/kubernetes-key.pem \
  --kubelet-https                   true \
  --runtime-config                  api/all,admissionregistration.k8s.io/v1alpha1 \
  --service-account-key-file        $SNAP_DATA/keychain/ca-key.pem \
  --service-cluster-ip-range        ${CONFIG[k8s.service.subnet]} \
  --service-node-port-range         30000-32767 \
  --tls-cert-file                   $SNAP_DATA/keychain/kubernetes.pem \
  --tls-private-key-file            $SNAP_DATA/keychain/kubernetes-key.pem \
  --v                               2
