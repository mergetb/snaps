#!/bin/bash

. $SNAP/bin/configure.sh

require       ip
require       index
require       ha
require_file  $SNAP_DATA/keychain/kubernetes.pem
require_file  $SNAP_DATA/keychain/kubernetes-key.pem
require_file  $SNAP_DATA/keychain/ca.pem
require_dir   $SNAP_COMMON/db

name=ma${CONFIG[index]}
if [[ ${CONFIG[ha]} == "yes" ]]; then
  cluster="ma0=https://ma0:2380,ma1=https://ma1:2380,ma2=https://ma2:2380"
else
  cluster="ma0=https://ma0:2380"
fi

$SNAP/bin/etcd                                                                          \
  --name                        $name                                                   \
  --cert-file                   $SNAP_DATA/keychain/db.pem                              \
  --key-file                    $SNAP_DATA/keychain/db-key.pem                          \
  --peer-cert-file              $SNAP_DATA/keychain/db.pem                              \
  --peer-key-file               $SNAP_DATA/keychain/db-key.pem                          \
  --trusted-ca-file             $SNAP_DATA/keychain/ca.pem                              \
  --peer-trusted-ca-file        $SNAP_DATA/keychain/ca.pem                              \
  --peer-client-cert-auth                                                               \
  --client-cert-auth                                                                    \
  --initial-advertise-peer-urls https://$name:2380                                      \
  --advertise-client-urls       https://$name:2379                                      \
  --listen-peer-urls            https://${CONFIG[ip]}:2380                              \
  --listen-client-urls          https://${CONFIG[ip]}:2379,http://127.0.0.1:2379        \
  --initial-cluster-token       etcd-cluster-0                                          \
  --initial-cluster             $cluster                                                \
  --initial-cluster-state       new                                                     \
  --data-dir                    $SNAP_COMMON/db                                         \
  --max-request-bytes           1073741824

