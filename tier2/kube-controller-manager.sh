#!/bin/bash

. $SNAP/bin/configure.sh

require   k8s.cluster.subnet #cluster_subnet
require   k8s.service.subnet #service_subnet

$SNAP/bin/kube-controller-manager \
  --address                              0.0.0.0 \
  --cluster-cidr                         ${CONFIG[k8s.cluster.subnet]} \
  --cluster-name                         kubernetes \
  --cluster-signing-cert-file            $SNAP_DATA/keychain/ca.pem \
  --cluster-signing-key-file             $SNAP_DATA/keychain/ca-key.pem \
  --leader-elect=true \
  --master                               http://127.0.0.1:8080 \
  --root-ca-file                         $SNAP_DATA/keychain/ca.pem \
  --service-account-private-key-file     $SNAP_DATA/keychain/ca-key.pem \
  --service-cluster-ip-range             ${CONFIG[k8s.service.subnet]} \
  --v                                    2
