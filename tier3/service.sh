#!/bin/bash 

while [[ ! -f $SNAP_DATA/configured ]]; do
  echo "waiting for configuration"
  sleep 1
done

export PATH="$SNAP/usr/sbin:$SNAP/usr/bin:$SNAP/sbin:$SNAP/bin:$PATH"
