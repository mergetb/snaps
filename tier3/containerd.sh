#!/bin/bash

. $SNAP/bin/service.sh

mkdir -p /run/merge-t3
export XDG_RUNTIME_DIR=/run/merge-t3

$SNAP/bin/containerd \
  --config $SNAP_DATA/etc/containerd-config.toml
