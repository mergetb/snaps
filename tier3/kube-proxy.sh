#!/bin/bash

. $SNAP/bin/service.sh

cluster_subnet="$(snapctl get cluster-subnet)"
echo "cluster_subnet: $cluster_subnet"

$SNAP/bin/kube-proxy \
  --cluster-cidr=$cluster_subnet \
  --kubeconfig=$SNAP_DATA/kube-proxy/kubeconfig \
  --feature-gates=SupportIPVSProxyMode=true \
  --proxy-mode=iptables \
  --v=4
