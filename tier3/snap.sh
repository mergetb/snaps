#!/bin/bash

snapcraft clean -s build
snapcraft clean wrappers -s pull
snapcraft clean configs -s pull
snapcraft clean runc -s pull
snapcraft clean iptables -s pull
snapcraft clean iptables-bins -s pull
snapcraft clean libnftnl -s pull
snapcraft
snapcraft clean
