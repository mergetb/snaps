#!/bin/bash

set -e

docker build -t snap-builder .
docker run -v `pwd`:/stuff -w /stuff snap-builder /bin/bash -c "apt-get update && ./snap.sh"
docker run -v `pwd`:/stuff -w /stuff snap-builder /bin/bash -c "chown $UID ./*.snap && chmod go+r ./*.snap"
