#!/bin/bash

. service.sh

keepalived \
  --dont-fork \
  -f $SNAP_DATA/keepalived/keepalived.conf

