#!/bin/bash

. service.sh

# -db is to run haproxy in foreground (required by snapd simple service model)
haproxy \
 -f $SNAP_DATA/haproxy/haproxy.cfg  \
 -db
  
