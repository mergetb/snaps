#!/bin/bash

set -e

# Get required configuration parameters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

declare -A CONFIG

require() {
  x="$(snapctl get $1)"
  while [[ $x == "" ]]; do
    echo "must provide $1"
    x="$(snapctl get $1)"
    sleep 1
  done
  CONFIG[$1]=$x
  echo "$1: $x"
}

require internal.ifx
require internal.floating
require internal.anchor
require internal.vrrp.passwd

require external.ifx
require external.floating.api
require external.floating.xdc
require external.floating.launch
require external.floating.wg
require external.anchor.api
require external.anchor.xdc
require external.anchor.launch
require external.anchor.wg
require external.vrrp.api.passwd
require external.vrrp.xdc.passwd
require external.vrrp.launch.passwd
require external.vrrp.wg.passwd

require k8s.nets.cni.subnet
#require k8s.nets.svc.subnet
require k8s.workers.count

require vrrp.mode

# Configure keepalived ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

mkdir -p $SNAP_DATA/keepalived

if [[ ${CONFIG[vrrp.mode]} == primary ]]; then

cat > $SNAP_DATA/keepalived/keepalived.conf <<EOF
vrrp_instance INTERNAL {
  interface ${CONFIG[internal.ifx]}
  state MASTER
  priority 200
  virtual_router_id 47
  virtual_ipaddress {
    ${CONFIG[internal.floating]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[internal.vrrp.passwd]}
  }
}

vrrp_instance API {
  interface ${CONFIG[external.ifx]}
  state MASTER
  priority 200
  virtual_router_id 74
  virtual_ipaddress {
    ${CONFIG[external.floating.api]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.api.passwd]}
  }
}

vrrp_instance XDC {
  interface ${CONFIG[external.ifx]}
  state MASTER
  priority 200
  virtual_router_id 99
  virtual_ipaddress {
    ${CONFIG[external.floating.xdc]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.xdc.passwd]}
  }
}

vrrp_instance LAUNCH {
  interface ${CONFIG[external.ifx]}
  state MASTER
  priority 200
  virtual_router_id 123
  virtual_ipaddress {
    ${CONFIG[external.floating.launch]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.launch.passwd]}
  }
}

vrrp_instance WG {
  interface ${CONFIG[external.ifx]}
  state MASTER
  priority 200
  virtual_router_id 147
  virtual_ipaddress {
    ${CONFIG[external.floating.wg]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.wg.passwd]}
  }
}
EOF

fi

if [[ ${CONFIG[vrrp.mode]} == secondary ]]; then

cat > $SNAP_DATA/keepalived/keepalived.conf <<EOF
vrrp_instance INTERNAL {
  interface ${CONFIG[internal.ifx]}
  state BACKUP
  priority 100
  virtual_router_id 47
  virtual_ipaddress {
    ${CONFIG[internal.floating]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[internal.vrrp.passwd]}
  }
}

vrrp_instance API {
  interface ${CONFIG[external.ifx]}
  state BACKUP
  priority 100
  virtual_router_id 74
  virtual_ipaddress {
    ${CONFIG[external.floating.api]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.api.passwd]}
  }
}

vrrp_instance XDC {
  interface ${CONFIG[external.ifx]}
  state BACKUP
  priority 100
  virtual_router_id 99
  virtual_ipaddress {
    ${CONFIG[external.floating.xdc]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.xdc.passwd]}
  }
}

vrrp_instance LAUNCH {
  interface ${CONFIG[external.ifx]}
  state BACKUP
  priority 100
  virtual_router_id 123
  virtual_ipaddress {
    ${CONFIG[external.floating.launch]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.launch.passwd]}
  }
}

vrrp_instance WG {
  interface ${CONFIG[external.ifx]}
  state BACKUP
  priority 100
  virtual_router_id 147
  virtual_ipaddress {
    ${CONFIG[external.floating.wg]}/24
  }
  authentication {
    auth_type PASS
    auth_pass ${CONFIG[external.vrrp.wg.passwd]}
  }
}
EOF

fi

# Configure haproxy ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

mkdir -p $SNAP_DATA/haproxy
mkdir -p $SNAP_COMMON/var/run
mkdir -p $SNAP_COMMON/var/lib/haproxy
cat > $SNAP_DATA/haproxy/haproxy.cfg <<EOF
global
    log         127.0.0.1 local2
    chroot      $SNAP_COMMON/var/lib/haproxy
    pidfile     $SNAP_COMMON/var/run/haproxy.pid
    maxconn     4000
    user        root
    group       root
    daemon
    stats socket $SNAP_COMMON/var/run/haproxy-stats.sock
    ssl-default-bind-ciphers PROFILE=SYSTEM
    ssl-default-server-ciphers PROFILE=SYSTEM

defaults
    mode                    tcp 
    log                     global
    #option                  httplog
    option                  dontlognull
    #option                  http-server-close
    #option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    maxconn                 3000
    timeout connect         3s
    timeout client          60s
    timeout server          60s

frontend kube-api-front
    bind ${CONFIG[internal.anchor]}:6443 
    default_backend kube-api-back

backend kube-api-back
    balance     roundrobin
    server      k8s-master0 ma0:6443 check
    server      k8s-master1 ma1:6443 check
    server      k8s-master2 ma2:6443 check

frontend k8-etcd-front
    bind ${CONFIG[internal.anchor]}:2379
    mode tcp
    default_backend k8-etcd-back

backend k8-etcd-back
    mode tcp
    balance     roundrobin
    server      k8s-db0 ma0:2379 check
    server      k8s-db1 ma1:2379 check
    server      k8s-db2 ma2:2379 check

frontend merge-front
    bind ${CONFIG[external.anchor.api]}:443
    default_backend merge-back

backend merge-back
    balance roundrobin
    server  merge-api0 mapi0:443 check
    server  merge-api1 mapi1:443 check
    server  merge-api2 mapi2:443 check

frontend xdc-ssh-front
    timeout connect         300s
    timeout client          300s
    timeout server          300s
    bind ${CONFIG[external.anchor.xdc]}:2202
    default_backend xdc-ssh-back

backend xdc-ssh-back
    timeout connect         300s
    timeout client          300s
    timeout server          300s
    balance roundrobin
    server  xdc-ssh0 xssh0:2202 check
    server  xdc-ssh1 xssh1:2202 check
    server  xdc-ssh2 xssh2:2202 check

frontend xdc-web-front
    timeout connect         300s
    timeout client          300s
    timeout server          300s
    bind ${CONFIG[external.anchor.xdc]}:443
    default_backend xdc-web-back

backend xdc-web-back
    timeout connect         300s
    timeout client          300s
    timeout server          300s
    balance roundrobin
    server  xdc-web0 xweb0:443 check
    server  xdc-web1 xweb1:443 check
    server  xdc-web2 xweb2:443 check

frontend launch-front
    bind ${CONFIG[external.anchor.launch]}:443
    default_backend launch-back

backend launch-back
    balance roundrobin
    server  launch0 launch0:443 check
    server  launch1 launch1:443 check
    server  launch2 launch2:443 check

frontend wg-front
    bind ${CONFIG[external.anchor.wg]}:6000
    default_backend wg-back

backend wg-back
    balance roundrobin
    server  wg0 wg0:6000 check
EOF

# Configure nftables
rmmod iptables_nat || echo "iptable_nat not loaded"
echo "net.ipv4.conf.all.route_localnet = 1" >> /etc/sysctl.conf
sysctl -p
mkdir -p $SNAP_DATA/nftables
cat > $SNAP_DATA/nftables/nftables.rules <<EOF
#!$SNAP/usr/sbin/nft -f

flush ruleset

# a simple static NAT that ensure all packets traversing anchor interfaces
# appear to come from virtual interfaces

flush ruleset

table ip nat {
  chain prerouting {
    type nat hook prerouting priority 0; policy accept;
    ip daddr ${CONFIG[internal.floating]} tcp dport 6443 dnat ${CONFIG[internal.anchor]}:6443
    ip daddr ${CONFIG[external.floating.api]} tcp dport 443 dnat ${CONFIG[external.anchor.api]}:443
    ip daddr ${CONFIG[external.floating.xdc]} tcp dport 443 dnat ${CONFIG[external.anchor.xdc]}:443
    ip daddr ${CONFIG[external.floating.xdc]} tcp dport 2202 dnat ${CONFIG[external.anchor.xdc]}:2202
    ip daddr ${CONFIG[external.floating.launch]} tcp dport 443 dnat ${CONFIG[external.anchor.launch]}:443
    ip daddr ${CONFIG[external.floating.wg]} tcp dport 6000 dnat ${CONFIG[external.anchor.wg]}:6000
  }
  chain postrouting {
  type nat hook postrouting priority 100; policy accept;
    ip saddr ${CONFIG[internal.anchor]} tcp sport 6443 snat ${CONFIG[internal.floating]}:6443
    ip saddr ${CONFIG[external.anchor.api]} tcp sport 443 snat ${CONFIG[external.floating.api]}:443
    ip saddr ${CONFIG[external.anchor.xdc]} tcp sport 443 snat ${CONFIG[external.floating.xdc]}:443
    ip saddr ${CONFIG[external.anchor.xdc]} tcp sport 2202 snat ${CONFIG[external.floating.xdc]}:2202
    ip saddr ${CONFIG[external.anchor.launch]} tcp sport 443 snat ${CONFIG[external.floating.launch]}:443
    ip saddr ${CONFIG[external.floating.wg]} tcp sport 6000 snat ${CONFIG[external.anchor.wg]}:6000
  }
}
EOF
chmod +x $SNAP_DATA/nftables/nftables.rules
$SNAP_DATA/nftables/nftables.rules

# Configure network addresses and routes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# internal and external addresses
#ip addr replace ${CONFIG[external.ip]} ${CONFIG[external.ifx]} 
#ip addr replace ${CONFIG[internal.ip]} ${CONFIG[internal.ifx]} 

# add a /32 route to each workers container network interface (cni)
ip route replace ${CONFIG[k8s.nets.cni.subnet]} dev ${CONFIG[internal.ifx]}

# add a /32 route to each workers external service address
#ip route replace ${CONFIG[k8s.nets.svc.subnet]} dev ${CONFIG[internal.ifx]}

# subnet=${CONFIG[k8s.nets.svc.subnet]}
# IP=(${subnet//./ })
# for i in `seq 1 ${CONFIG[k8s.workers.count]}`; do
#   ip route replace ${IP[0]}.${IP[1]}.$i.1/32 dev ${CONFIG[internal.ifx]}
# done

touch $SNAP_DATA/configured
echo "configuration complete"
