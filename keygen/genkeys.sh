#!/bin/bash

set -e

if [[ ! -f cfssl ]]; then
  curl -o cfssl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  chmod +x cfssl
fi

if [[ ! -f cfssljson ]]; then
  curl -o cfssljson -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x cfssljson
fi

# Define the following externally according to your needs
DOMAIN=${DOMAIN:-mergetb.net}
WORKERS=${WORKERS:-kw0 kw1 kw2 kw3 kw4 kw5 kw6 kc0 kc1 kc2 ks0 ks1 ks2}
CLUSTERIP=${CLUSTERIP:-10.77.0.1}

# generate certificate authority
if [[ ! -f ca.pem ]]; then
echo "generating ca"
./cfssl gencert -initca ca-csr.json | ./cfssljson -bare ca
fi

# generate k8s api keys
# TODO possibly need the service_cider here (10.77.0.1 default)
if [[ ! -f kubernetes.pem ]]; then
echo "generating kubernetes"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -hostname=px,px0,px1,ma0,ma1,ma2,$CLUSTERIP \
  -profile=mergetb                            \
  kubernetes-csr.json                         \
  | ./cfssljson -bare kubernetes
fi

# generate merge api keys
# TODO should be replaced with letsencrypt certs in production
if [[ ! -f merge.pem ]]; then
echo "generating merge"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -hostname=api.$DOMAIN                       \
  -profile=mergetb                            \
  merge-api-csr.json                          \
  | ./cfssljson -bare merge
fi

# generate client admin cert
if [[ ! -f admin.pem ]]; then
echo "generating admin"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -profile=mergetb                            \
  admin-csr.json                              \
  | ./cfssljson -bare admin
fi

# generate kube-proxy keys
if [[ ! -f kube-proxy.pem ]]; then
echo "generating kube-proxy"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -profile=mergetb                            \
  kube-proxy-csr.json                         \
  | ./cfssljson -bare kube-proxy
fi

# generate k8s worker keys
for x in $WORKERS; do

if [[ ! -f $x.pem ]]; then
cat > /tmp/$x-csr.json <<EOF
{
  "CN": "system:node:$x",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "system:nodes",
      "OU": "mergetb portal"
    }
  ]
}
EOF

echo "generating kw"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -hostname=$x                                \
  -profile=mergetb                            \
  /tmp/$x-csr.json                            \
  | ./cfssljson -bare $x
fi

done

# generate k8 db keys
if [[ ! -f db.pem ]]; then
echo "generating db"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -hostname=ma0,ma1,ma2                       \
  -profile=mergetb                            \
  db-csr.json | ./cfssljson -bare db
fi

# generate merge db keys
if [[ ! -f mdb.pem ]]; then
echo "generating db"
./cfssl gencert                               \
  -ca=ca.pem                                  \
  -ca-key=ca-key.pem                          \
  -config=ca-config.json                      \
  -hostname=db0,db1,db2,db                    \
  -profile=mergetb                            \
  db-csr.json | ./cfssljson -bare mdb
fi

mkdir keychain
cp *.pem keychain/
tar czf merge-keychain.tgz keychain
rm -rf keychain
